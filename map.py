import os
import sys
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
# mapclassify
# descartes

root = sys.path[0]

shapefile = f'{root}/shp/ne_10m_admin_0_countries_lakes/ne_10m_admin_0_countries_lakes.shp'
# gdf = gpd.read_file(shapefile).to_crs('+proj=robin')
gdf = gpd.read_file(shapefile)[['ADM0_A3', 'NAME', 'geometry']].to_crs('+proj=robin')
print(gdf.sample(5))
# print(gdf.columns)

df = pd.read_json(f'{root}/public/permillion.json')
# df = df.assign(pop=df.totalcases / df.tot_cases_1m_pop)
# df = df.assign(serious_per_m=df.serious_critical / df['pop'])

# print(df.columns)
print(df.sample(5))

merged = gdf.merge(df, left_on='ADM0_A3', right_on='cca3')

# merged = merged[merged['population'] >= 1]

# print(merged.sample(5))
# print(merged[merged.isna().any(axis=1)])

ax = merged.plot(column='deaths', cmap='coolwarm', figsize=(16, 10))

# merged[merged.isna().any(axis=1)].plot(ax=ax, color='#fafafa', hatch='///')

plt.savefig(f'{root}/public/map.png')

# print(merged.isna().sample(5))
