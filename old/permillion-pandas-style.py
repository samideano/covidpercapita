import sys
import pandas as pd
import seaborn as sns

columns = ['cases', 'new_cases', 'deaths', 'new_deaths', 'recovered', 'active', 'serious']
number_format = "{:,.2f}"
formatting = {i: number_format for i in columns}

html_string = '''
<html>
  <head><title>COVID-19 numbers per capita</title></head>
  <link href="https://unpkg.com/tabulator-tables@4.6.0/dist/css/tabulator.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="permillion.css"/>
  <script src="https://unpkg.com/tabulator-tables@4.6.0/dist/js/tabulator.min.js"></script>
  <script src="permillion.js"></script>
  <body>
    {table}
  </body>
</html>.
'''

df = pd.read_json(sys.path[0] + '/worldometer.json')

df.rename(inplace=True, columns={
    'country_other': 'country',
    'totalcases': 'cases',
    'newcases': 'new_cases',
    'totaldeaths': 'deaths',
    'newdeaths': 'new_deaths',
    'totalrecovered': 'recovered',
    'activecases': 'active',
    'serious_critical': 'serious'
})

df['population'] = df.cases / df.tot_cases_1m_pop
del df['tot_cases_1m_pop']
del df['deaths_1m_pop']
del df['1stcase']

df.new_cases.fillna('0', inplace=True)
df.new_deaths.fillna('0', inplace=True)

df.new_cases = df.new_cases.str.replace('+', '').str.replace(',', '').astype(int)
df.new_deaths = df.new_deaths.str.replace('+', '').str.replace(',', '').astype(int)

for c in columns:
    df[c] = df[c] / df.population

df = df[df['population'] >= 1]

df.sort_values('deaths', ascending=False, inplace=True)

# print(df.dtypes)
print(df.head(5))

html_table = (
    df.style
    .format(formatting)
    .set_properties(**{'font-size': '11pt', 'font-family': 'Monaco'})
    .set_table_styles([dict(selector="tr:hover", props=[("background-color", '#ccc')])])
    .highlight_null(null_color='#999')
    .bar(color='#f30')
    .hide_index()
    .render(uuid='permillion')
)

html_string = html_string.format(table=html_table)

with open(sys.path[0] + '/permillion.html', 'w') as f: f.write(html_string)
