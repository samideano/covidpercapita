# http://queirozf.com/entries/pandas-dataframe-examples-column-operations#apply-function-to-column-names
import sys
import pandas as pd
import seaborn as sns
from hdx.location.country import Country

root = sys.path[0]

countries = pd.read_csv(f'{root}/countries/countries.csv', usecols=['name', 'cca2', 'cca3', 'population', 'flag'])
countries.rename(columns={'name': 'country'}, inplace=True)
countries.population = countries.population / 1000

df = pd.read_json(f'{root}/worldometer.json')

df.rename(inplace=True, columns={
    'country_other': 'country',
    'totaltests': 'tests',
    'totalcases': 'cases',
    # 'newcases': 'new_cases',
    'totaldeaths': 'deaths',
    # 'newdeaths': 'new_deaths',
    'totalrecovered': 'recovered',
    'activecases': 'active',
    'serious_critical': 'serious'
})

del df['tests_1m_pop']
del df['tot_cases_1m_pop']
del df['deaths_1m_pop']
# del df['reported1st_case']
del df['newcases']
del df['newdeaths']

exceptions = {'UK': 'GBR', 'CAR': 'CAF'}
df['cca3'] = df.country.map(lambda fuzzy: exceptions.get(fuzzy) or Country.get_iso3_country_code_fuzzy(fuzzy)[0])
del df['country']

df = df.merge(countries, left_on='cca3', right_on='cca3')
# print(df.sample(5))

# df.new_cases.fillna(0, inplace=True)
# df.new_deaths.fillna(0, inplace=True)

for c in ['tests', 'cases', 'deaths', 'recovered', 'active', 'serious']:
    df[c] = df[c] / df.population

# df = df[df['population'] >= 1]
# df.sort_values('deaths', ascending=False, inplace=True)

# print(df.dtypes)
print(df.head(5))

df.to_json(f'{root}/public/permillion.json', orient='records', indent=2)
