# run with: pipenv run python worldometer.py
import sys
import re
import pandas as pd
import requests
from bs4 import BeautifulSoup
import simplejson as json
from urllib.parse import unquote
from slugify import slugify

response = requests.get('https://www.worldometers.info/coronavirus/')

df = pd.read_html(response.text)[0]

df.columns = df.columns.map(lambda x: slugify(x, separator='_'))

print(df[0:10])

df.to_json(sys.path[0] + '/worldometer.json', orient='records', indent=2, force_ascii=False)
