import sys
import pandas as pd
import flag

root = sys.path[0]

# Source: https://worldpopulationreview.com/countries/
codes = pd.read_csv(f'{root}/codes.csv', na_filter=False)
population = pd.read_csv(f'{root}/population.csv')

countries = codes.merge(population, left_on='name', right_on='name')
del countries['pop2018']
countries.rename(inplace=True, columns={
    'pop2019': 'population',
    'Rank': 'rank',
    'Density': 'density',
    'GrowthRate': 'growth_rate'
})

print('Problematic rows:')
print(countries[countries.isna().any(axis=1)])

countries['flag'] = countries.cca2.map(lambda code: flag.flag(code))

print(countries.sample(5))
print(countries.dtypes)

countries.to_csv(f'{root}/countries.csv')
