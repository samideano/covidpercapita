document.addEventListener('DOMContentLoaded', () => {
  let boilerplate = (obj) => {
    obj.headerSortStartingDir = 'desc'
    obj.sorter = 'number'
    // obj.frozen = true

    obj.formatter = 'money'
    obj.formatterParams = {precision: 1}
    // obj.formatter = 'progress'
    // obj.formatterParams = {legend: true}

    return obj
  }

  let tabulator = new Tabulator('#permillion', {
    height: '100%',
    ajaxURL: 'permillion.json',
    selectable: true,
    initialFilter: [{field: 'population', type: '>=', value: 1}],
    initialSort: [{column: 'deaths', dir: 'desc'}],
    columns: [
      {formatter: 'rownum', frozen: true},
      {title: 'Country', field: 'country', frozen: true, formatter: (cell) => {
        row = cell.getRow().getData()
        return `${row.flag} ${row.country}`
      }},
      // {title: 'Country Code', field: 'country_code', frozen: true},
      boilerplate({title: 'Tests', field: 'tests'}),
      boilerplate({title: 'Cases', field: 'cases'}),
      // boilerplate({title: 'New Cases', field: 'new_cases'}),
      boilerplate({title: 'Deaths', field: 'deaths'}),
      // boilerplate({title: 'New Deaths', field: 'new_deaths'}),
      boilerplate({title: 'Recovered', field: 'recovered'}),
      boilerplate({title: 'Active', field: 'active'}),
      boilerplate({title: 'Serious', field: 'serious'}),
      boilerplate({title: 'Population (M)', field: 'population'})
    ]
  })
})
